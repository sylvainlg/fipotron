<?php
/**
 *	This file is part of LighTemplate
 *
 *	LighTemplate is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *	
 *	LighTemplate is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *	
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Olivier Cecillon
 * @copyright &copy; 2007 - Olivier Cecillon
 * @license http://www.fsf.org/licensing/licenses/gpl.html
 * @version 0.4
 */

class LighTemplateFileException extends LighTemplateException {
}

?>