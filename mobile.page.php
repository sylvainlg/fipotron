<?php

//$pad = file_get_contents('http://piratepad.net/ep/pad/export/fipotron/latest?format=txt');

//var_dump(parse_ini_string($pad, true));
//$pad_ini = preg_replace('#Section ([0-9]+)#', 'Section_\1', $pad);

//$tab = parse_ini_string_perso($pad, true);

require_once('cache.php');

$tab = Cache :: getCachedFile();
$keys = array_keys($tab);

$tpl = new Tpl('./tpl');
	

$nb_poss = 1;
$size = sizeof($keys);
for($i=0;$i<$size;++$i) {
	$nb_poss *= sizeof($tab[$keys[$i]]);
}

#####################################
####### Génération du Javascript ####
#####################################

$genepipo = '';

for($i = 0;$i<$size;++$i)
{
	$genepipo .= 'i = document.forme.pipo' .$i.'.selectedIndex ;' . "\n";
	$genepipo .= 'chaine'.$i.' = document.forme.pipo'.$i.'.options[i].value + " " ;' . "\n";
	$genepipo .= 'chaine += chaine' .$i . ";\n";
}

$hasard = '';

for($i=0;$i<$size;++$i) {
	$hasard .= 'i = Math.random() ;' . "\n";
	$hasard .= 'i = Math.floor(' . sizeof($tab[$keys[$i]]) . '*i) ;' . "\n";
	$hasard .= 'document.forme.pipo'.$i.'.selectedIndex = i ;' . "\n";
}

$tpl->assign('genepipo', $genepipo);
$tpl->assign('hasard', $hasard);

$main -> assign('js', $tpl -> fetch('mobile.js.tpl'));

######################################
####### Génération des listes ########
######################################

$tpl = new Tpl('./tpl');

$listes = '';

for($i=0;$i<$size;++$i) {
	$s = sizeof($tab[$keys[$i]]);
	$listes .= '<select name="pipo'.$i.'" size="'.$s.'" onchange="genepipo()" >' . "\n";
	for($j=0;$j<$s;++$j) {
		$txt = $tab[$keys[$i]]['p'.($j+1)];
		$txt = preg_replace('/"/', '&quot;', $txt);
		$listes .= '<option value="' . $txt . '" selected="selected">' . $txt . '</option>' . "\n";
	}
	$listes .= '</select><br /><br />' . "\n";
}

$tpl->assign('nb_poss', $nb_poss);
$tpl->assign('listes', $listes);

$main -> assign('page', $tpl->fetch('index.tpl'));

?>
