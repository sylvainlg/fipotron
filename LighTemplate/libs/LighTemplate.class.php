<?php
/**
 *	LighTemplate
 *	Copyright (C) 2008  Olivier Cecillon
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *	
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Olivier Cecillon
 * @copyright &copy; 2007 - Olivier Cecillon
 * @license http://www.fsf.org/licensing/licenses/gpl.html
 * @version 0.4
 */

require_once(dirname(__FILE__) . '/LighTemplateException.class.php');
require_once(dirname(__FILE__) . '/LighTemplateFileException.class.php');
require_once(dirname(__FILE__) . '/LighTemplateDirException.class.php');

/**
 * @package Lightemplate
 */
class LighTemplate {
	
	/**
	 * The directory where the template system will find templates to parse
	 * @var string
	 */
	public $template_dir;
	
	/**
	 * The format for template variables
	 * @var string
	 */
	public $var_format = '{_%s_}';
	
	/**
	 * Template variables : the key in templates
	 * @var array
	 */
	protected $aSearchVars = array();
	
	/**
	 * Template variables : the values to replace the keys with
	 * @var array
	 */
	protected $aReplaceVars = array();
	
	/**
	 * The class constructor
	 * @param string $template_dir The template directory
	 */
	public function __construct($template_dir = './templates') {
		# Use specified template directory, else use default one
		if (is_string($template_dir) && $template_dir !== '') {
			if (is_dir($template_dir) && is_readable($template_dir)) {
				$this -> template_dir = realpath($template_dir);
			}
			else {
				if (!is_readable($template_dir)) {
					throw new LighTemplateDirException('Template directory ' . $template_dir . ' is not readable', LighTemplate::READ_ERROR);
				}
				else {
					throw new LighTemplateDirException('Directory ' . $template_dir . ' does not exist', LighTemplate::NOT_FOUND);
				}
			}
		}
		elseif ($template_dir === '') {
				throw new LighTemplateException('Empty name given. LighTemplate cannot work without a valid template directory', LighTemplateException::EMPTY_NAME);
		}
		else {
			throw new InvalidArgumentException('Invalid argument for contructor. String expected, ' . gettype($template_dir) . ' given');
		}
	}
	
	/**
	 * Appends a string to a variable. If variable has not been
	 * defined, it is created then
	 * @since Version 0.4
	 * @param scalar $sVar The name of the variable
	 * @param scalar $sValue The value to append to the variable
	 * @return boolean True if success, false if not
	 */
	public function append($sVar, $sValue) {
		if (is_scalar($sFormatedVar = $this -> formatVar($sVar) && is_scalar($sValue))) {
			if (isset($this -> aReplace[$sFormatedVar])) {
				$this -> aReplace[$sFormatedVar] .= $sValue;
			}
			else {
				$this -> aSearch[$sFormatedVar] = $sFormatedVar;
				$this -> aReplace[$sFormatedVar] = $sValue;
			}
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Return a parsed file, without displaying it
	 * @param string $sFile The file to parse
	 * @param array $aLocalVars An associative array of local variables for the template
	 * @return string Parsed string
	 */
	public function fetch($sFile, array $aLocalVars = array()) {
		$aSearch = array();
		if ($aLocalVars !== array()) {
			foreach ($aLocalVars as $varKey => $varValue) {
				if (is_scalar($varValue)) {
					$sFormatedVar = $this -> formatVar($varKey);
					$aSearch[$sFormatedVar] = $sFormatedVar;
				}
			}
		}
		# Read file (if readable)
		if ($sContents = @file_get_contents($this -> template_dir . '/' . $sFile)) {
			$sParsedContents = $this -> parse($sContents, $aSearch, $aLocalVars, false);
			return $sParsedContents;
		}
		#  File not found or not readable
		else {
			if (!is_file($this -> template_dir . '/' . $sFile)) {
				throw new LighTemplateFileException('File ' . $sFile . ' not found in ' . $this -> template_dir . ', or file is a directory', LighTemplateFileException::NOT_FOUND);
			}
			else {
				throw new LighTemplateFileException('File ' . $sFile . ' in ' . $this -> template_dir . ' is not readable', LighTemplateFileException::READ_ERROR);
			}
		}
	}
	
	/**
	 * Fetch and parse a file, iterating on array
	 * The file will be parsed as many times as the array has elements
	 * When merging, specific Template Variables will override Template Global Variables
	 * @param string $sFile The file to fetch and parse
	 * @param mixed $mData An array or an iterator containing the elements of the loop
	 * @param boolean $bMerge Flag : whether to merge specific template vars with global vars
	 * @return string Parsed string
	 */
	public function fetchLoop($sFile, Iterator $mData, $bMerge = false) {
		# Fetch first element to read keys
		$mDataArray = $mData -> current();
		if (!is_array($mDataArray)) {
			throw new InvalidArgumentException('Invalid Iterator : iterator should return arrays when iterating, ' . gettype($mDataArray) . ' returned.');
		}
		$aKeys = array_keys($mDataArray);
		# Rewind iterator to be ready to iterate in loop
		$mData -> rewind();
		
		# Format the search vars
		foreach($aKeys as $k => $v) {
			if (is_scalar($v)) {
				$fV = $this -> formatVar($v);
				$aSearch[$fV] = $fV;
			}
		}
		
		# Read file (if readable)
		if ($sContents = @file_get_contents($this -> template_dir . '/' . $sFile)) {
			$sParsedContents = '';
			# Loop to parse
			foreach ($mData as $aVars) {
				$sParsedContents .= $this -> parse($sContents, $aSearch, $aVars, $bMerge);
			}
			return $sParsedContents;
		}
		# File not found or not readable
		else {
			if (!is_file($this -> template_dir . '/' . $sFile)) {
				throw new LighTemplateFileException('File ' . $sFile . ' not found in ' . $this -> template_dir . ', or file is a directory', LighTemplateFileException::NOT_FOUND);
			}
			else {
				throw new templateException('File ' . $sFile . ' not readable');			
			}
		}
	}
	
	/**
	 * Apply format to Search Variable
	 * @param string $sVar
	 * @return string|mixed Formated $sVar if string, $sVar if not
	 */
	protected function formatVar($sVar) {
		if (is_string($sVar)) {
			return sprintf($this -> var_format, $sVar);
		}
		else {
			return $sVar;
		}
	}

	/**
	 * Parse a template file
	 * When merging, specific Template Variables may override Template Global Variables
	 * @param string $sContents Contents of the file
	 * @param array $aSearch Optional specific SearchVars for the template
	 * @param array $aReplace Optional specific ReplaceVars for the template
	 * @param boolean Flag : whether to merge template variables or not
	 * @return string Parsed string
	 */
	public function parse($sContents = '', array $aSearch = array(), array $aReplace = array(), $bMerge = false) {
		if ($aSearch === array() || $aReplace === array()) {
			$aSearch = $this -> aSearchVars;
			$aReplace = $this -> aReplaceVars;
		}
		elseif ($bMerge) {
			$aSearch = array_merge($this -> aSearchVars, $aSearch);
			$aReplace = array_merge($this -> aReplaceVars, $aReplace);
		}
		return str_replace($aSearch, $aReplace, $sContents);
	}
	
	/**
	 * Assigns values to template variables, in order to replace them when parsing file
	 * @param mixed $mVar Can be a string or an array
	 * @param string $sValue Optional argument, required only if $mVar is a string
	 * @param boolean $bOverride Whether to override variables which would have already been assigned
	 */ 
	public function assign($mVar, $sValue = null, $bOverride = true) {
		# Assign simple couple (var, value)
		if (is_scalar($mVar) && $mVar !== '') {
				$mFormatedVar = $this -> formatVar($mVar);
				if (!isset($this -> aSearchVars[$mFormatedVar]) || $bOverride) {
					$this -> aSearchVars[$mFormatedVar] = $mFormatedVar;
					$this -> aReplaceVars[$mFormatedVar] = $sValue;
				}
		}
		# Assign array of couples
		elseif (is_array($mVar) && $mVar !== array()) {
			foreach ($mVar as $varKey => $varValue) {
				# Only assign sscalar values
				if (is_scalar($varValue)) {
					$sFormatedVar = $this -> formatVar($varKey);
					# Should we override if variable already assigned
					if (!isset($this -> aSearchVars[$sFormatedVar]) || $bOverride) {
						$this -> aSearchVars[$sFormatedVar] = $sFormatedVar;
						$this -> aReplaceVars[$sFormatedVar] = $varValue;
					}
				}
			}
		}
	}

	/**
	 * Destroy a Template Variable
	 * @param string $sVar The name of the variable to destroy
	 */
	public function clear($sVar = null) {
		if ($sValue == null) {
			$this -> aVars = array();
			$this -> aSearchVars = array();
			$this -> aReplaceVars = array();
		}
		elseif (is_string($sVar)) {
			$sFormatedVar = $this -> formatVar($sVar);
			unset($this -> aSearchVar[$sFormatedVar], $this -> aReplaceVar[$sFormatedVar]);
		}
	}

}

?>