<?php

	$pages = array('jsonexport');
	
	$p = $_GET['p'];
		
	if(in_array($p, $pages)) {
		require_once($p . '.page.php');
	} else {
		echo 'Page not found.';
	}

	exit;
	
?>
