<?php

require_once(dirname(__FILE__).'/cache.php');

if(isset($_GET['what']) && 'dujour' === $_GET['what']) {
    echo file_get_contents('./cache/fipotrondujour') ;
    
} else {
    

    $tab = Cache :: getCachedFile();
    $keys = array_keys($tab);
    $size = sizeof($keys);

    $random = '';

    for($i=0;$i<$size;++$i) {
        $rand = rand( 1, sizeof($tab[$keys[$i]]) );
        $random .= $tab[$keys[$i]]['p'.$rand] . ' ';
    }

    echo $random;

}

exit;