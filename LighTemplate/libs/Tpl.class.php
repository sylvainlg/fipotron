<?php

require_once('LighTemplate/libs/LighTemplate.class.php');

class Tpl extends LighTemplate {

	/**
	 * The format for template variables
	 * @var string
	 */
	public $var_format = '{$%s}';

	/**
	* Assign the value to the key if key doesn't existing yet using the assign function
	*/
	public function assignIfNone($mVar, $sValue = null) {
		$this->assign($mVar, $sValue, false);
	}

}

?>
