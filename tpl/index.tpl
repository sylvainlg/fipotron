		<h2>Qu'est-ce que le fipotron ?</h2>

		<p>Il s'agit d'un outil formidable qui fournit la solution à tous les FIPs en manque d'inspiration. Avant toute rencontre avec la FIP ou même pour vos cours du quotidien, n'oubliez pas d'avoir quelques phrases pipo à votre disposition.</p>

		<h2>Je veux modifier le fipotron ?</h2>

		<p>Rien de plus facile, tu te rends à l'adresse <a href="http://piratepad.net/fipotron">http://piratepad.net/fipotron</a> et tu rajoutes ton pipo.</p>

		<h2>A ce jour il y a {$nb_poss} combinaisons possibles.</h2>

		<hr />

		<form method="POST" action="" name="forme">

		<textarea name="texte" cols="80" rows="8" wrap="soft" style="color:black; background-color:#FFFFCC; font-size:1.1em; ">Cliquez sur le bouton pour une composition automatique,
		ou composez manuellement votre pipo
		en choisissant dans les listes ci-dessous.
		</textarea>

		<div align="left">
			<input type="button" value="Génération automatique, au hasard" onclick="hasard()" />
		</div>

		{$listes}

		</form>
