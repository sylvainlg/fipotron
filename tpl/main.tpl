<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title>Fipotron</title>
 
    <!-- meta -->
    <meta name="description" content="fipotron FIP" />
    <meta name="keywords" content="fip TB" />
    <meta name="author" content="sylvainlg" />
 
    <!-- mon icon -->
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- ma template.css -->
    <link href="{$css}" type="text/css" rel="stylesheet" media="screen" charset="utf-8" />
    
    {$js}
 
</head>
<body>

	<h1>Fipotron</h1>

	<div id="nav">
		<ul>
			<li><a href="http://fipotron.fipiniere.fr/">Accueil</a></li>
			<li><a href="http://fipotron.fipiniere.fr/?p=dujour">Du jour</a></li>
			<li><a href="http://fipotron.fipiniere.fr/?p=modif">Modifier</a></li>
		</ul>
	</div>

	<div id="page">

		{$page}

	</div>

</body>
</html>
