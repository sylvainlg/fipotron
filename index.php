<?php

	$pages = array('index', 'modif', 'dujour');
	
	$p = $_GET['p'];
	
	require_once dirname(__FILE__) . '/browscap/Browscap.php';

	$bc = new phpbrowscap\Browscap(dirname(__FILE__) . '/cache');
	
	// Gets information about the current browser's user agent
	$b = $bc->getBrowser();
	
//	var_dump($b->isMobileDevice);
//	echo ($b->isMobileDevice !== true) ? 'Non mobile' : 'Mobile';
/*
	// Output the result
	echo '<pre>'; // some formatting issues ;)
	print_r($current_browser);
	echo '</pre>';
*/

	require_once('LighTemplate/libs/Tpl.class.php');
	
	$main = new Tpl(dirname(__FILE__) . '/tpl');
	$main -> var_format = '{$%s}';

	
	if($b->isMobileDevice === true) {
	//if(true) { // debug
		
		// Bonjour, c'est un mobile qui arrive chez toi ;)
		
		$main->assign('css', 'mobile.css');

		require_once('mobile.page.php');
		
		echo $main->fetch('mobile.tpl');
		
		exit;
		
	}
	
	
	if(empty($p))
		require_once('index.page.php');
	elseif (in_array($p, $pages)) {
		require_once($p . '.page.php');
	} else {
		$main->assign('page', 'Page not found.');
	}
	
	$main -> assignIfNone('css', 'index.css');
	$main -> assignIfNone('js', '');
	$main -> assignIfNone('page', '');
	
	echo $main->fetch('main.tpl');
	exit;
	
?>
