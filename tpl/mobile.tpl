<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title>Fipotron</title>
 
    <!-- meta -->
    <meta name="description" content="fipotron FIP" />
    <meta name="keywords" content="fip TB" />
    <meta name="author" content="sylvainlg" />
 
    <!-- mon icon -->
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- ma template.css -->
    <link href="mobile.css" type="text/css" rel="stylesheet" media="screen" charset="utf-8" />
    
    {$js}
 
</head>
<body>

	<h1>Fipotron Mobile</h1>
	
	<button onclick="hasard()" type="button">Générer</button>
	
	<p id="texte">Cliquez sur le bouton pour générer un fipotron.</p>

	<div id="page">

		{$page}

	</div>

</body>
</html>
