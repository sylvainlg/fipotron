<?php

class Cache  {
	
	private static $_folder = './cache/';
	
	private static $_file = 'piratpad';

/*
	public getFolder() {
		return $this -> _folder;
	}
	
	public setFolder($folder) {
		$this -> _folder = $folder;
	}
*/

	public static function getCachedFile() {

		$file = self::$_folder . self::$_file ;
		$cache_life = 60 * 60; //caching time, in seconds
		
		if( !file_exists($file) || (time() - filemtime($file) >= $cache_life) ) {
			@$pad = file_get_contents('http://piratepad.net/ep/pad/export/fipotron/latest?format=txt');
//var_dump($pad);
			if($pad) {// ! enleve
				file_put_contents($file, $pad);
			} else {
				$pad = file_get_contents(dirname(__FILE__) . '/cache/fipotron-latest.txt');
			}
		} else {
			$pad = file_get_contents($file);
		}
		
		if($pad == false || empty($pad)) {
			$pad = file_get_contents(dirname(__FILE__) . '/cache/fipotron-latest.txt');
		}
		
		return Cache :: parse ( $pad );

	}

	protected static function parse ( $content ) {
	
		return Cache :: parse_ini_string_perso( $content, true);
	
	}

	private static function parse_ini_string_perso($str, $ProcessSections=false) {
	
		$lines  = explode("\n", $str);
		$return = Array();
		$inSect = false;
		foreach($lines as $line){
		    $line = trim($line);
		    if(!$line || $line[0] == "#" || $line[0] == ";")
		        continue;
		    if($line[0] == "[" && $endIdx = strpos($line, "]")){
		        $inSect = substr($line, 1, $endIdx-1);
		        continue;
		    }
		    if(!strpos($line, '=')) // (We don't use "=== false" because value 0 is not valid as well)
		        continue;
		    
		    $tmp = explode("=", $line, 2);
		    $tmp[1] = substr($tmp[1], 0, strlen($tmp[1]));
		    if($ProcessSections && $inSect)
		        $return[$inSect][trim($tmp[0])] = ltrim($tmp[1]);
		    else
		        $return[trim($tmp[0])] = ltrim($tmp[1]);
		}
		return $return;
		
	}


}

?>
