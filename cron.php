<?php

require_once(dirname(__FILE__).'/cache.php');

$tab = Cache :: getCachedFile();
$keys = array_keys($tab);

$size = sizeof($keys);

//var_dump($tab);

$random = '';

for($i=0;$i<$size;++$i) {
	$rand = rand( 1, sizeof($tab[$keys[$i]]) );
	$random .= $tab[$keys[$i]]['p'.$rand] . ' ';
}

//echo $random;

// send it by mail
//mail();

$headers ='From: "Fipotron du jour"<fipotron@fipiniere.fr>'."\n";
$headers .='Reply-To: fip2012@resel.fr'."\n";
$headers .='Content-Type: text/plain; charset="utf-8"'."\n";
$headers .='Content-Transfer-Encoding: 8bit'; 

mail('fip2012@resel.fr', 'Fipotron du jour - ' . date('d/m/Y'), $random . "\n\n\n--\nhttp://fipotron.fipiniere.fr/dujour", $headers);

// save it into cache
file_put_contents(dirname(__FILE__) . '/cache/fipotrondujour', $random);

?>
